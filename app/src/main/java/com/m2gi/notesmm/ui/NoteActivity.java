package com.m2gi.notesmm.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.m2gi.notesmm.NoteApp;
import com.m2gi.notesmm.R;
import com.m2gi.notesmm.data.Note;
import com.m2gi.notesmm.data.NoteUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Locale;

public class NoteActivity extends AppCompatActivity implements
        RecognitionListener, OrientationEventManager.OrientationEventListener  {
    private static final int REQUEST_RECORD_PERMISSION = 100;

    TextToSpeech t1;

    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";
    private TextView returnedText;
    private int nbEtape;
    private Note note;
    private EditText noteTitle;
    private EditText noteContent;
    private Context context;
    private String result="";
    ImageView img;

    private OrientationEventManager orientationEventManager;
    private boolean isShakeDetectionEnabled;
    private boolean isOrientationDetectionEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        orientationEventManager = new OrientationEventManager(NoteApp.getAppContext(), this);

        isOrientationDetectionEnabled = true;

        context = getApplicationContext();

        note = (Note) getIntent().getSerializableExtra("Note");
        Intent installIntent = new Intent();
        installIntent.setAction(
                TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
        startActivity(installIntent);
        t1=new TextToSpeech(NoteActivity.this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                t1.setLanguage(Locale.FRANCE);
                t1.isLanguageAvailable(Locale.ENGLISH);
            }
        });
        //displayReconization();
        nbEtape=1;
        noteContent = findViewById(R.id.noteContent);
        noteContent.setText(note.getContent());

        Toolbar toolbar = findViewById(R.id.noteToolbar);
        noteTitle = new EditText(this);
        noteTitle.setText(note.getTitle());
        noteTitle.setTextSize(30);
        noteTitle.setTypeface(Typeface.DEFAULT_BOLD);
        noteTitle.setTextColor(Color.WHITE);
        noteTitle.setBackground(null);
        noteTitle.setHint("Titre");
        toolbar.addView(noteTitle);
        img= (ImageView) findViewById(R.id.imageView1);
        img.setImageResource(R.drawable.microphonehot);

        toolbar.inflateMenu(R.menu.menu_note);
        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNote();
                NoteUtil.saveNote(note);
                finish();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()){
                    case R.id.action_delete:
                        NoteUtil.deleteNote(note);
                        Toast.makeText(context, "Supprimé", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case R.id.action_save:
                        updateNote();
                        NoteUtil.saveNote(note);
                        Toast.makeText(context, "Sauvegardé", Toast.LENGTH_SHORT).show();
                        break;
                }

                return onOptionsItemSelected(item);
            }
        });
    }

    private void updateNote(){
        note.setTitle(noteTitle.getText().toString());
        note.setContent(noteContent.getText().toString());
        note.updateLastModified();
     //   displayReconization();
    }

    public void displayReconization(){
        if (speech != null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i(LOG_TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this));
            speech.setRecognitionListener(this);
            recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "fr");
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1000);
            ActivityCompat.requestPermissions(NoteActivity.this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_PERMISSION);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_PERMISSION:
                if (speech != null && grantResults.length > 0 && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                    speech.startListening(recognizerIntent);
                } else {
                    //Toast.makeText(NoteActivity.this, "Permission Denied!", Toast .LENGTH_SHORT).show();
                }
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        displayReconization();
    }
    @Override
    public void onResume() {

        super.onResume();
       displayReconization();
    }
    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onStop() {
        super.onStop();
//        if (nbEtape==2){
        noteContent.setSelection(noteContent.getText().length());
//            nbEtape=0;
//        }

    }
    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
    }
    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }
    @Override
    public void onEndOfSpeech() {
        Log.i(LOG_TAG, "onEndOfSpeech");
//        if (nbEtape==2){
        noteContent.setSelection(noteContent.getText().length());
//            nbEtape=0;
//        }
        //if (nbEtape==2){
             onRestart();
        //}
    }
    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(LOG_TAG, "FAILED " + errorMessage);
    }
    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Log.i(LOG_TAG, "onEvent");
    }
    @Override
    public void onPartialResults(Bundle arg0) {
        Log.i(LOG_TAG, "onPartialResults");
    }
    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");
    }
    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text = result;
        if (nbEtape==1){
            noteTitle.setText(text);
            text=" ";
            nbEtape=2;
        }
//        noteContent.setSelection(0,noteContent.getText().length());
        if (text.contains("enregistrées") || text.contains("enregistrés") ){
            updateNote();
            NoteUtil.saveNote(note);
            Toast.makeText(context, "Sauvegardé", Toast.LENGTH_SHORT).show();
            t1.speak("note Sauvegardé", TextToSpeech.QUEUE_ADD, null,null);

        }else if(text.contains("Supprimé") || text.contains("supprimé")) {
            NoteUtil.deleteNote(note);
            Toast.makeText(context, "Supprimé", Toast.LENGTH_SHORT).show();
            t1.speak("note Supprimé", TextToSpeech.QUEUE_ADD, null,null);
            finish();
        }else if(text.contains("voix off") || text.contains("voix of") || text.contains("voie of") || text.contains("voie off")) {
            stopReconization();
            // enable orientaion change detection
            isOrientationDetectionEnabled = true;
            img.setImageResource(R.drawable.microphonedesactivate);

        } else {
                result=result+" "+text;
                if(result != null && nbEtape==2)
                    noteContent.setText(result);
        }


//        noteContent.setSelection(noteContent.getText().length());

    }
    @Override
    public void onRmsChanged(float rmsdB) {
        Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
    }
    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    public void stopReconization(){
        speech.stopListening();
    }

    @Override
    public void onOrientationChange() {
        if (isOrientationDetectionEnabled) {
            // enable voice detection
            speech = SpeechRecognizer.createSpeechRecognizer(this);
            displayReconization();
            img.setImageResource(R.drawable.microphonehot);
            // disable orientation change detection
            isOrientationDetectionEnabled = false;
        }
    }
}