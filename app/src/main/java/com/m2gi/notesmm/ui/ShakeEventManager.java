package com.m2gi.notesmm.ui;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.m2gi.notesmm.NoteApp;

public class ShakeEventManager extends Activity implements SensorEventListener {

    private static final float ALPHA = (float) 0.2;
    private static final float MOV_THRESHOLD = (float) 0.5;
    private static final float SHAKE_WINDOW_TIME_INTERVAL = (float) 1000;
    private static final float MOV_COUNTS = (float) 3;
    private static int counter = 0;
    private static long firstMovTime = 0;

    private SensorManager sManager;
    private Sensor sensor;
    private float[] gravity = new float[3];
    private ShakeListener listener;

    public ShakeEventManager (Context context, ShakeListener listener) {
        this.listener = listener;
        sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        register();
    }

    public void register() {
        sManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float maxAcc = calcMaxAcceleration(event);
        if (maxAcc >= MOV_THRESHOLD) {
            if (counter == 0) {
                counter++;
                firstMovTime = System.currentTimeMillis();
            } else {
                long now = System.currentTimeMillis();
                if ((now - firstMovTime) < SHAKE_WINDOW_TIME_INTERVAL) {
                    counter++;
                } else {
                    counter = 0;
                    return;
                }
                if (counter >= MOV_COUNTS)
                    if (listener != null)
                        listener.onShake();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private float calcMaxAcceleration(SensorEvent event) {
        gravity[0] = calcGravityForce(event.values[0], 0);
        gravity[1] = calcGravityForce(event.values[1], 1);
        gravity[2] = calcGravityForce(event.values[2], 2);

        float accX = event.values[0] - gravity[0];
        float accY = event.values[1] - gravity[1];
        float accZ = event.values[2] - gravity[2];

        float max1 = Math.max(accX, accY);
        return Math.max(max1, accZ);
    }

    private float calcGravityForce(float currentVal, int index) {
        return  ALPHA * gravity[index] + (1 - ALPHA) * currentVal;
    }

    protected void onResume() {
        super.onResume();
        sManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sManager.unregisterListener(this);
    }
    
    public static interface ShakeListener {
        public void onShake();
    }
}
