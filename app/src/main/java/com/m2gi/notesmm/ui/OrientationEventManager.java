package com.m2gi.notesmm.ui;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.ImageView;

public class OrientationEventManager {

    private ImageView imageView;

    private SensorManager sensorManager;
    private Sensor sensorAccelerometer;
    private Sensor sensorMagneticField;
    private OrientationEventListener listener;

    private float[] floatGravity = new float[3];
    private float[] floatGeoMagnetic = new float[3];

    private float lastFloatOrientation;
    private float[] floatOrientation = new float[3];
    private float[] floatRotationMatrix = new float[9];

    protected OrientationEventManager(Context context, OrientationEventListener listener) {
        this.listener = listener;
        sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);

        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        SensorEventListener sensorEventListenerAccelrometer = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                floatGravity = event.values;

                SensorManager.getRotationMatrix(floatRotationMatrix, null, floatGravity, floatGeoMagnetic);
                SensorManager.getOrientation(floatRotationMatrix, floatOrientation);

                if (lastFloatOrientation - floatOrientation[0] > 2) {
                    listener.onOrientationChange();
                }
                lastFloatOrientation = floatOrientation[0];
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };

        SensorEventListener sensorEventListenerMagneticField = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                floatGeoMagnetic = event.values;

                SensorManager.getRotationMatrix(floatRotationMatrix, null, floatGravity, floatGeoMagnetic);
                SensorManager.getOrientation(floatRotationMatrix, floatOrientation);

                if (lastFloatOrientation - floatOrientation[0] > 2) {
                    listener.onOrientationChange();
                }
                lastFloatOrientation = floatOrientation[0];
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
        sensorManager.registerListener(sensorEventListenerAccelrometer, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListenerMagneticField, sensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public static interface OrientationEventListener {
        public void onOrientationChange();
    }

}