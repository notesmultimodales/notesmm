package com.m2gi.notesmm.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.m2gi.notesmm.NoteApp;
import com.m2gi.notesmm.R;
import com.m2gi.notesmm.data.Note;
import com.m2gi.notesmm.data.NoteUtil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

enum DisplayMode {
    CARDS,
    LIST
}

enum SortMethod {
    MODIFICATION_DATE,
    ALPHABETICAL,
    CREATION_DATE
}

enum SortOrder {
    ASC,
    DSC
}

public class MainActivity extends AppCompatActivity implements
        RecognitionListener, OrientationEventManager.OrientationEventListener, ShakeEventManager.ShakeListener {
    private static final int REQUEST_RECORD_PERMISSION = 1;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";
    private TextView returnedText;
    private ShakeEventManager shakeEventManager;
    private OrientationEventManager orientationEventManager;

    private boolean hasActivityStarted;
    private boolean isShakeDetectionEnabled;
    private boolean isOrientationDetectionEnabled;
    ImageView img;
    TextToSpeech t1;

    SortMethod sortMethod;
    DisplayMode displayMode;
    SortOrder sortOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent installIntent = new Intent();
        installIntent.setAction(
                TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
        startActivity(installIntent);
        t1=new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                t1.setLanguage(Locale.FRANCE);
                t1.isLanguageAvailable(Locale.ENGLISH);
            }
        });


        sortMethod = SortMethod.ALPHABETICAL;
        displayMode = DisplayMode.CARDS;
        sortOrder = SortOrder.ASC;

        // show phone menu simulator
        setContentView(R.layout.bg_simulator);
        // activate shake detection
        isShakeDetectionEnabled = true;

        //initiate sensors managers
        shakeEventManager = new ShakeEventManager(NoteApp.getAppContext(), this);
        orientationEventManager = new OrientationEventManager(NoteApp.getAppContext(), this);

    }

    public void initiateActivity() {
        hasActivityStarted = true;
        returnedText = findViewById(R.id.textView1);
        setContentView(R.layout.activity_main);

        img= (ImageView) findViewById(R.id.imageView1);
        img.setImageResource(R.drawable.microphonedesactivate);

        Toolbar toolbar = findViewById(R.id.listToolbar);
        toolbar.inflateMenu(R.menu.menu_main);
        toolbar.setOnMenuItemClickListener(item -> {
            switch(item.getItemId()){
                case R.id.action_card_mode:
                    displayMode = DisplayMode.CARDS;
                    Toast.makeText(getApplicationContext(), "Affichage en mode carte", Toast.LENGTH_SHORT).show();
                    loadList();
                    break;
                case R.id.action_list_mode:
                    displayMode = DisplayMode.LIST;
                    Toast.makeText(getApplicationContext(), "Affichage en mode liste", Toast.LENGTH_SHORT).show();
                    loadList();
                    break;
                case R.id.action_settings:

                    break;
                case R.id.action_sort_order:
                    if(sortOrder == SortOrder.ASC){
                        sortOrder = SortOrder.DSC;
                    }else{
                        sortOrder = SortOrder.ASC;
                    }
                    loadList();
                    break;
                case R.id.sort_alpha:
                    sortMethod = SortMethod.ALPHABETICAL;
                    loadList();
                    break;
                case R.id.sort_creation_date:
                    sortMethod = SortMethod.CREATION_DATE;
                    loadList();
                    break;
                case R.id.sort_modification_date:
                    sortMethod = SortMethod.MODIFICATION_DATE;
                    loadList();
                    break;
            }

            return onOptionsItemSelected(item);
        });

        FloatingActionButton fab = findViewById(R.id.addNote);

        fab.setOnClickListener(view -> {
            //Create note
            Note note = new Note();
            NoteUtil.saveNote(note);
            Intent intent = new Intent(view.getContext(), NoteActivity.class);
            intent.putExtra("Note", note);
            startActivity(intent);
        });
        loadList();
    }
    public void displayReconization(){
        if (speech != null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i(LOG_TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this));
            speech.setRecognitionListener(this);
            recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,"fr");
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1000);
            ActivityCompat.requestPermissions (MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_PERMISSION);
        }
    }
    public void stopReconization(){
        speech.stopListening();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        displayReconization();
        if (hasActivityStarted) {
            loadList();
        }
    }

    private void loadList(){
        List<Note> notes = NoteUtil.getNotes();
        LinearLayout noteList = findViewById(R.id.noteList);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
        noteList.removeAllViewsInLayout();

        Comparator<Note> comparator = null;
        switch(sortMethod){
            case ALPHABETICAL:
                comparator = Comparator.comparing(Note::getTitle);
                break;
            case CREATION_DATE:
                comparator = Comparator.comparing(Note::getCreationDate);
                break;
            case MODIFICATION_DATE:
                comparator = Comparator.comparing(Note::getLastModified);
                break;
        }
        if(sortOrder == SortOrder.DSC){
            comparator = comparator.reversed();
        }
        notes.sort(comparator);

        for(final Note note : notes){
            final View view;

            if(displayMode == DisplayMode.CARDS) {
                view = LayoutInflater.from(this).inflate(R.layout.note_card, noteList, false);

                TextView title = view.findViewById(R.id.cardTitle);
                TextView content = view.findViewById(R.id.cardContentPreview);
                TextView creationDate = view.findViewById(R.id.cardCreationDate);

                title.setText(note.getTitle());
                if (note.getContent().length() > 100) {
                    content.setText(note.getContent().substring(0, 99) + "...");
                } else {
                    content.setText(note.getContent());
                }
                creationDate.setText("Créée: " + dateFormat.format(note.getCreationDate()) + "\nModifiée: " + dateFormat.format(note.getLastModified()));
            }else{
                view = LayoutInflater.from(this).inflate(R.layout.note_element_list, noteList, false);
                TextView title = view.findViewById(R.id.noteTitle);
                title.setText(note.getTitle());
            }
            view.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), NoteActivity.class);
                intent.putExtra("Note", note);
                startActivity(intent);
            });

            noteList.addView(view);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_PERMISSION:
                if (speech != null && grantResults.length > 0 && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                    speech.startListening(recognizerIntent);
                } else {
                    // Toast.makeText(MainActivity.this, "Permission Denied!", Toast .LENGTH_SHORT).show();
                }
        }
    }
    @Override
    public void onResume() {
        //speech.cancel();
        //speech.destroy();
        super.onResume();
        displayReconization();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();

    }
    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");


    }
    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }
    @Override
    public void onEndOfSpeech() {
//        Log.i(LOG_TAG, "onEndOfSpeech");

    }
    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(LOG_TAG, "FAILED " + errorMessage);
    }
    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Log.i(LOG_TAG, "onEvent");
    }
    @Override
    public void onPartialResults(Bundle arg0) {
        Log.i(LOG_TAG, "onPartialResults");


    }
    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");


    }
    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text = result + "\n";

        returnedText.setText(text);

        if (text.contains("créer une note") || text.contains("créée une note")){
                findViewById(R.id.addNote).callOnClick();
            t1.speak("Note créer", TextToSpeech.QUEUE_ADD, null,null);
                //speech.destroy();

        }else if(text.contains("lire") || text.contains("lear") || text.contains("Lear")) {
             if (!NoteUtil.getNotes().isEmpty()){
            String commande[]= text.split(" ");
            System.out.println(commande[0] + " commande 2 "+commande[1]);
            System.out.println(text);
            Log.d("Textttt ","---------------------> text "+ text);
            Log.d("commande  0","---------------------> comm 0 "+commande[0]);
            Log.d("commande  1","---------------------> comm 1 "+commande[1]);

                 Note note = NoteUtil.getNoteByTitle(commande[1]);
//                 returnedText.setText();
                if(note != null){
                    if(note.getContent() != null && !note.getContent().equals("")){
                        int ok = t1.speak(note.getContent(), TextToSpeech.QUEUE_ADD, null, null);
                        if (ok == 0){
                            returnedText.setText(text);
                        }else {
                            returnedText.setText("erreur");
                        }
                        onRestart();
                    }else {
                            t1.speak("Pourriez vous répéter s'il vous plait?", TextToSpeech.QUEUE_ADD, null,null);
                            onRestart();
                        }
                }else {
                    t1.speak("Il y aucune note à lire correspond à ce titre, penser à créer une avant !", TextToSpeech.QUEUE_ADD, null,null);
//                    onRestart();
                }
            }else {
                t1.speak("Il y aucune note à lire, penser à créer une avant !", TextToSpeech.QUEUE_ADD, null,null);
//                onRestart();
            }
        }else if (text.contains("carte") || text.contains("part") || text.contains("car tu")) {
            displayMode = DisplayMode.CARDS;
            Toast.makeText(getApplicationContext(), "Affichage en mode carte", Toast.LENGTH_SHORT).show();
            loadList();
            t1.speak("Affichage en mode carte", TextToSpeech.QUEUE_ADD, null,null);

        }else if(text.contains("liste")) {
            displayMode = DisplayMode.LIST;
            Toast.makeText(getApplicationContext(), "Affichage en mode liste", Toast.LENGTH_SHORT).show();
            loadList();
            t1.speak("Affichage en mode liste", TextToSpeech.QUEUE_ADD, null,null);

        }else if(text.contains("voix off") || text.contains("voix of") || text.contains("voie of") || text.contains("voie off")) {
            speech=null;
            // enable orientaion change detection
            isOrientationDetectionEnabled = true;
            img.setImageResource(R.drawable.microphonedesactivate);
        }{
                onRestart();
        }
    }
    @Override
    public void onRmsChanged(float rmsdB) {


        Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
    }
    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    @Override
    public void onShake() {
        Log.d("SwA", "*********************************************************************");
        if (isShakeDetectionEnabled) {
            // start activity
            initiateActivity();

            // disable shake detection
            isShakeDetectionEnabled = false;

            // enable orientation change detection
            isOrientationDetectionEnabled = true;
        }
    }

    @Override
    public void onOrientationChange() {
        if (isOrientationDetectionEnabled) {
            // enable voice detection
            speech = SpeechRecognizer.createSpeechRecognizer(this);
            t1.speak("micro activé", TextToSpeech.QUEUE_ADD, null,null);
            img.setImageResource(R.drawable.microphonehot);

            // disable orientation change detection
            isOrientationDetectionEnabled = false;
        }
    }


}