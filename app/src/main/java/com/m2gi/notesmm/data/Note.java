package com.m2gi.notesmm.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class Note implements Serializable {
    private static final long serialVersionUID = 100L;
    String title;
    String content;
    String fileName;
    Date creationDate;
    Date lastModified;

    public Note() {
        this.title = "";
        this.content = "";
        this.fileName = null;
        this.creationDate = new Date();
        this.lastModified = new Date();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void updateLastModified(){
        lastModified = new Date();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
