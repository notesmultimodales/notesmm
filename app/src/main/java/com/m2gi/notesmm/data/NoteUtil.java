package com.m2gi.notesmm.data;

import com.m2gi.notesmm.NoteApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.UUID;

public class NoteUtil {

    public static ArrayList<Note> getNotes(){
        try {
            File dir = NoteApp.getAppContext().getDataDir();
            File notesDir = new File(dir.getAbsoluteFile() + "/notes");
            if(!notesDir.isDirectory()) {
                notesDir.mkdir();
            }
            ArrayList<Note> notes = new ArrayList();
            for(File file : notesDir.listFiles()) {
                ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
                Note note = (Note) objectInputStream.readObject();
                objectInputStream.close();
                notes.add(note);
            }
            return notes;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<Note>();
    }

    public static void saveNote(Note note){
        try {
            File dir = NoteApp.getAppContext().getDataDir();
            File notesDir = new File(dir.getAbsoluteFile() + "/notes");
            if(note.getFileName() == null){
                note.setFileName(UUID.randomUUID().toString());
            }
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(notesDir.getPath() + "/" + note.getFileName()));
            objectOutputStream.writeObject(note);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteNote(final Note note){
        File dir = NoteApp.getAppContext().getDataDir();
        File notesDir = new File(dir.getAbsoluteFile() + "/notes");
        File[] files = notesDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String name) {
            return name.equals(note.getFileName());
            }
        });
        if(files.length > 0){
            files[0].delete();
        }
    }

    public static Note getNoteByTitle(String title){
        ArrayList<Note> notes= getNotes();
        if (notes.isEmpty()){
            return null;
        }else {
            for (Note note : notes) {
                if (note.getTitle().equals(title)){
                    return note;
                }
            }
        }
        return null;
    }

}
