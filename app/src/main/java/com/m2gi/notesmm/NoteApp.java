package com.m2gi.notesmm;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.util.Log;

import com.m2gi.notesmm.ui.ShakeEventManager;
import android.os.Vibrator;
public class NoteApp extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        NoteApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return NoteApp.context;
    }

}



